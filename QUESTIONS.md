# Questions

Qs1: Explain the output of the following code and why

```js
setTimeout(function () {
  console.log("1");
}, 100);
console.log("2");
```

An1: the above code will print the following in the console:
```js
2
1
```
The JavaScript "setTimeout" method accepts 2 mandatory parameters:

1. a function or expression
2. an interval after which to execute the function or evauate the expression, expressed in milliseconds
   Optional parameters could be passed to the "setTimeout" method that will be passed to the anonymous function.
   The execution starts evaluating the setTimeout method and starts the 100ms timer. In the meantime the next
   instruction will be evaluated, which is to print "2" in the console. The 100ms timeout will the expire and
   the anonymous function will be executed, printing "1" in the console.
   "setTimeout" is an example of an asynchronous method.
   It may be interesting to note that the output would be the same even if we passed a timeout of zero.
   This is because asynchronous code is always executed after the main execution flow has completed.

Qs2: Explain the output of the following code and why

```js
function foo(d) {
  if (d < 10) {
    foo(d + 1);
  }
  console.log(d);
}
foo(0);
```

An2: the above code will print the numbers from 10 to 0 in the console, in descending order.
This is an example of iterative excution where the function call starts the flow by passing 0 to the
"foo" function. The "if" statement evaluates the condition 0 < 10 and, since this evaluates to "true"
executes the code in the body of the statement. This contains a function call, so at this point the
execution of the current instance of "foo" is paused and another one is started, to which the parameter
0+1 is passed. This process will continue iteratively, pausing and nesting execution flows until the "if" statement
evaluates to "false", which is when the parameter equals 10. At this point the normal flow of the "innermost"
function will move on to the next statement, which is to print 10 to the console. Execution being completed
at this point, the execution flow will be passed back to the calling function - which is an instance of
"foo" to which the parameter 9 was passed. This instance will in turn execute its next statement, printing
9, and then pass the control back to its calling function. The process will continue until the original
calling instance with a parameter of 0 will complete its execution.

Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
function foo(d) {
  d = d || 5;
  console.log(d);
}
```

An3: the logical operator "OR" (||) in JS returns the value to its left if it's "truthy", and the value
to its right if the left value is "falsy". In JS falsy values are "undefined", "null", "false", "NaN", an empty
string and the number 0. The potential issue is that we may want to pass anothe falsy value, (0 for example) as a valid parameter
to our function. But since 0 is a falsy value, 5 would be assigned to d instead and printed in the console.
A way to avoid the issue would be to use the nullish coalescing operator (??) in place of "OR". This will return the right-hand value
only if the left-hand value is strictly null or undefined. This is probably the desirable behavior, since calling
foo() equals to foo(undefined).
An even stricter option would be to assign a default to the parameter, which is used only if the function call has no parameters
or the parameter equals "undefined":

```js
function foo(d = 5) {
  console.log(d);
}
```

In this case, foo(0) will log 0 to the console, foo(null) would log "null" and foo() would log "5".

Qs4: Explain the output of the following code and why

```js
function foo(a) {
  return function (b) {
    return a + b;
  };
}
var bar = foo(1);
console.log(bar(2));
```

An4: this code will log "3" in the console. The var assignment sets the value of bar to the value returned by the function "foo"
executed with a parameter equal to 1. The value returned by "foo" is another function that takes a single parameter and returns the sum
of this last parameter and the one with which "foo" was originally called. As a consequence, after

```js
var bar = foo(1);
```

we have:

```js
bar = function (b) {
  return 1 + b;
};
```

At this point is easy to understand that bar(2) returns 3. In this case "bar" is an example of closure with a lexical environment
of 1. "foo" is essentially a "function factory" that we can use to create other functions that add specific values to a parameter.
Example:

```js
var fooBar = foo(5);
console.log(fooBar(2));
```

will log "7" and so on.

Qs5: Explain how the following function would be used

```js
function double(a, done) {
  setTimeout(function () {
    done(a * 2);
  }, 100);
}
```

An5: if we inspect the body of the timeout method, we see that it calls a "done" function with the result of "a\*2". Both "a"
and "done" are received by the function as parameters. If follows that "double" has to be called with a value (possibly a
number, otherwise we will get a NaN result) and a function. So one possible valid call would be:

```js
double(2, console.log);
```

which will log "4" to the console after a 100ms timeout. We need to be aware of the asynchronous context of execution here, as
everything in the main flow will run before the body of the "setTimeout" method (see An1). This is always the challenge with the
asynchronous code, as it may be difficult or impossible to pinpoint at which exact point of our execution flow it will be run.
